package com.guillervm.spacexrockets.data.model

import android.os.Parcel
import android.os.Parcelable

data class Engines(val number: Int) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(number)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Engines> {
        override fun createFromParcel(parcel: Parcel): Engines {
            return Engines(parcel)
        }

        override fun newArray(size: Int): Array<Engines?> {
            return arrayOfNulls(size)
        }
    }
}