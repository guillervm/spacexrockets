package com.guillervm.spacexrockets.data.service

import com.guillervm.spacexrockets.data.model.Launch
import com.guillervm.spacexrockets.data.model.Rocket
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface SpaceXService {

    @GET("rockets")
    fun getRockets(): Observable<List<Rocket>>

    @GET("launches")
    fun getAllLaunchesForRocket(@Query("rocket_id") rocketId: String): Observable<List<Launch>>
}