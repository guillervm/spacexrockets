package com.guillervm.spacexrockets.data.model

import android.os.Parcel
import android.os.Parcelable

data class Launch(
    val missionName: String,
    val launchYear: String,
    val launchDateUnix: Long,
    val launchSuccess: Boolean?,
    val links: LaunchLinks
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readParcelable(LaunchLinks::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(missionName)
        parcel.writeString(launchYear)
        parcel.writeLong(launchDateUnix)
        parcel.writeValue(launchSuccess)
        parcel.writeParcelable(links, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Launch> {
        override fun createFromParcel(parcel: Parcel): Launch {
            return Launch(parcel)
        }

        override fun newArray(size: Int): Array<Launch?> {
            return arrayOfNulls(size)
        }
    }
}