package com.guillervm.spacexrockets.data.api

import io.reactivex.Scheduler
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.lang.reflect.Type

class CustomCallAdapterFactory private constructor() : CallAdapter.Factory() {

    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *>? {
        return CustomCallAdapterWrapper.wrap(adapterFactory.get(returnType, annotations, retrofit))
    }

    companion object {
        private lateinit var adapterFactory: RxJava2CallAdapterFactory

        fun create(): CustomCallAdapterFactory {
            adapterFactory = RxJava2CallAdapterFactory.create()
            return CustomCallAdapterFactory()
        }

        fun createWithScheduler(scheduler: Scheduler): CustomCallAdapterFactory {
            adapterFactory = RxJava2CallAdapterFactory.createWithScheduler(scheduler)
            return CustomCallAdapterFactory()
        }
    }
}