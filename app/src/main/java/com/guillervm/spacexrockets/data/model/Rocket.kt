package com.guillervm.spacexrockets.data.model

import android.os.Parcel
import android.os.Parcelable

data class Rocket(
    val id: Long,
    val rocketId: String,
    val rocketName: String,
    val description: String,
    val country: String,
    val active: Boolean,
    val engines: Engines,
    var launches: List<Launch>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readParcelable(Engines::class.java.classLoader),
        parcel.createTypedArrayList(Launch)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(rocketId)
        parcel.writeString(rocketName)
        parcel.writeString(description)
        parcel.writeString(country)
        parcel.writeByte(if (active) 1 else 0)
        parcel.writeParcelable(engines, flags)
        parcel.writeTypedList(launches)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Rocket> {
        override fun createFromParcel(parcel: Parcel): Rocket {
            return Rocket(parcel)
        }

        override fun newArray(size: Int): Array<Rocket?> {
            return arrayOfNulls(size)
        }
    }

}