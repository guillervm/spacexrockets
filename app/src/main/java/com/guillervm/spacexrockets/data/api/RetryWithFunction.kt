package com.guillervm.spacexrockets.data.api

import io.reactivex.Observable
import io.reactivex.functions.Function
import java.io.IOException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit

class RetryWithFunction(private val maxRetries: Int) : Function<Observable<out Throwable>, Observable<*>> {

    private var retryCount = 0

    override fun apply(observable: Observable<out Throwable>): Observable<*> {
        return observable.flatMap { throwable ->
            if (++retryCount < maxRetries && throwable is IOException) {
                // Connection lost, timeout or something similar, retry if max retries not reached
                Observable.timer(if (throwable is UnknownHostException) 1000 else 0, TimeUnit.MILLISECONDS)
            } else {
                // Max retries hit or other kind of error, pass the error along
                Observable.error<Any>(throwable)
            }
        }
    }

}