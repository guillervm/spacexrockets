package com.guillervm.spacexrockets.data.api

import io.reactivex.Observable
import retrofit2.Call
import retrofit2.CallAdapter
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

class CustomCallAdapterWrapper<T> private constructor(callAdapter: CallAdapter<T, Any>?) : CallAdapter<T, Any> {

    private val adapter: CallAdapter<T, Any>? = callAdapter

    override fun responseType(): Type? {
        return adapter?.responseType()
    }

    override fun adapt(call: Call<T>): Any? {
        var adapted: Any? = adapter?.adapt(call)
        if (adapted is Observable<*>) {
            adapted = adapted.timeout(15, TimeUnit.SECONDS).retryWhen(RetryWithFunction(3))
        }
        return adapted
    }

    companion object {
        fun wrap(callAdapter: CallAdapter<*, *>?): CustomCallAdapterWrapper<*> {
            return CustomCallAdapterWrapper(callAdapter as CallAdapter<out Any, Any>?)
        }
    }
}