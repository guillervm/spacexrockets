package com.guillervm.spacexrockets.data.model

import android.os.Parcel
import android.os.Parcelable

data class LaunchLinks(
    val missionPatch: String?,
    val missionPatchSmall: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(missionPatch)
        parcel.writeString(missionPatchSmall)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LaunchLinks> {
        override fun createFromParcel(parcel: Parcel): LaunchLinks {
            return LaunchLinks(parcel)
        }

        override fun newArray(size: Int): Array<LaunchLinks?> {
            return arrayOfNulls(size)
        }
    }
}