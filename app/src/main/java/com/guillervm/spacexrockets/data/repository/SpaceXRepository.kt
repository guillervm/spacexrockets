package com.guillervm.spacexrockets.data.repository

import com.guillervm.spacexrockets.data.model.Launch
import com.guillervm.spacexrockets.data.model.Rocket
import com.guillervm.spacexrockets.data.service.SpaceXService
import com.guillervm.spacexrockets.ioc.annotation.ApplicationScope
import io.reactivex.Observable
import javax.inject.Inject

@ApplicationScope
class SpaceXRepository @Inject constructor(private val spaceXService: SpaceXService) {

    fun getRockets(): Observable<List<Rocket>> = spaceXService.getRockets()

    fun getAllLaunchesForRocket(rocketId: String): Observable<List<Launch>> =
        spaceXService.getAllLaunchesForRocket(rocketId)
}