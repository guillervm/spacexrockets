package com.guillervm.spacexrockets.data.local

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class PreferencesHelper @Inject constructor(context: Context) {

    private lateinit var preferences: SharedPreferences

    init {
        preferences = context.getSharedPreferences(preferencesFileKey, Context.MODE_PRIVATE)
    }

    fun getWelcomeMessageDisplayed(): Boolean {
        return preferences.getBoolean(preferenceWelcomeMessage, false)
    }

    fun setWelcomeMessageDisplayed() {
        val editor = preferences.edit()
        editor.putBoolean(preferenceWelcomeMessage, true)
        editor.apply()
    }

    companion object {
        const val preferencesFileKey = "general_preferences"
        const val preferenceWelcomeMessage = "preference_welcome_message"
    }

}