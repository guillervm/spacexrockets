package com.guillervm.spacexrockets.ioc.module

import com.guillervm.spacexrockets.ioc.annotation.ActivityScope
import com.guillervm.spacexrockets.view.details.DetailsActivity
import com.guillervm.spacexrockets.view.details.DetailsActivityModule
import com.guillervm.spacexrockets.view.main.MainActivity
import com.guillervm.spacexrockets.view.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [DetailsActivityModule::class])
    abstract fun bindDetailsActivity(): DetailsActivity
}