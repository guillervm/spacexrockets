package com.guillervm.spacexrockets.ioc.module

import android.content.Context
import com.guillervm.spacexrockets.SpaceXRocketsApplication
import dagger.Binds
import dagger.Module

@Module(includes = [ApiServiceModule::class])
abstract class AppModule {

    @Binds
    abstract fun bindContext(application: SpaceXRocketsApplication): Context
}