package com.guillervm.spacexrockets.ioc.module

import com.guillervm.spacexrockets.data.service.SpaceXService
import com.guillervm.spacexrockets.ioc.annotation.ApplicationScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module(includes = arrayOf(ApiModule::class))
class ApiServiceModule {

    @Provides
    @ApplicationScope
    fun provideApi(retrofit: Retrofit): SpaceXService {
        return retrofit.create(SpaceXService::class.java)
    }
}