package com.guillervm.spacexrockets.ioc

import com.guillervm.spacexrockets.SpaceXRocketsApplication
import com.guillervm.spacexrockets.ioc.annotation.ApplicationScope
import com.guillervm.spacexrockets.ioc.module.ActivityModule
import com.guillervm.spacexrockets.ioc.module.AppModule
import com.guillervm.spacexrockets.ioc.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule


@ApplicationScope
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ViewModelModule::class,
        ActivityModule::class
    ]
)
internal interface AppComponent : AndroidInjector<SpaceXRocketsApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<SpaceXRocketsApplication>()
}