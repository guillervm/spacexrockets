package com.guillervm.spacexrockets.util

import android.content.Context
import android.net.ConnectivityManager
import java.lang.ref.WeakReference
import javax.inject.Inject

class NetworkUtil @Inject constructor(context: Context) {

    var context = WeakReference<Context>(context)

    fun isConnected(): Boolean {
        var validContext = context.get()
        if (validContext != null) {
            var manager = validContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            var networkInfo = manager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }
        return false
    }
}