package com.guillervm.spacexrockets.util

import java.util.*

class Debouncer(private val debounceTime: Long = 500L, val f: () -> Unit) {

    private var lastAction = 0L

    fun action() {
        if (Calendar.getInstance().timeInMillis > lastAction + debounceTime) {
            lastAction = Calendar.getInstance().timeInMillis
            f()
        }
    }
}