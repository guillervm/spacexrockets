package com.guillervm.spacexrockets.util

import android.content.res.Resources
import android.util.DisplayMetrics

class DisplayUtils {

    companion object {
        fun dpToPixels(dp: Float): Float {
            return dp * Resources.getSystem().displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT
        }
    }
}