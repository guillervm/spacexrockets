package com.guillervm.spacexrockets.util

import android.content.Context
import android.content.Intent
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import com.guillervm.spacexrockets.R

class DialogFactory {

    companion object {

        fun displayWelcomeDialog(context: Context) {
            AlertDialog.Builder(context)
                .setMessage(R.string.welcome_text)
                .setPositiveButton(R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .show()
        }

        fun displayGenericErrorDialog(context: Context) {
            AlertDialog.Builder(context)
                .setMessage(R.string.generic_error_text)
                .setPositiveButton(R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .show()
        }

        fun displayNoInternetDialog(context: Context) {
            AlertDialog.Builder(context)
                .setMessage(R.string.no_internet_text)
                .setPositiveButton(R.string.settings) { _, _ ->
                    context.startActivity(Intent(Settings.ACTION_WIRELESS_SETTINGS))
                }
                .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss() }
                .create()
                .show()
        }

    }

}