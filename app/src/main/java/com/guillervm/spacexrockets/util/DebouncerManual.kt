package com.guillervm.spacexrockets.util

import java.util.*

class DebouncerManual(private val debounceTime: Long = 500L) {

    private var lastAction = 0L

    fun action(): Boolean {
        if (Calendar.getInstance().timeInMillis > lastAction + debounceTime) {
            lastAction = Calendar.getInstance().timeInMillis
            return true
        }
        return false
    }
}