package com.guillervm.spacexrockets.view.main

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class NoRocketsPlaceholderViewHolder(val view: View) : RecyclerView.ViewHolder(view)