package com.guillervm.spacexrockets.view.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.guillervm.spacexrockets.R
import com.guillervm.spacexrockets.common.BaseActivity
import com.guillervm.spacexrockets.data.model.Rocket
import com.guillervm.spacexrockets.util.DebouncerManual
import com.guillervm.spacexrockets.util.DialogFactory
import com.guillervm.spacexrockets.view.details.DetailsActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), PopupMenu.OnMenuItemClickListener {

    enum class Filter {
        ACTIVE, INACTIVE, ALL
    }

    private val detailsRequestCode = 0
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var mainViewModel: MainViewModel
    private var filter = Filter.ALL

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        observeLoader(mainViewModel)

        val adapterClickDebouncer = DebouncerManual()
        val adapter = MainAdapter {
            if (adapterClickDebouncer.action()) {
                startActivityForResult(DetailsActivity.newIntent(this, it), detailsRequestCode)
            }
        }

        rv_content.layoutManager = LinearLayoutManager(this).apply { orientation = RecyclerView.VERTICAL }
        rv_content.adapter = adapter
        val ta = obtainStyledAttributes(intArrayOf(R.attr.actionBarSize))
        val toolbarHeight = ta.getDimensionPixelSize(0, -1)
        ta.recycle()
        rv_content.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            val scrollableSpace = resources.getDimension(R.dimen.main_logo_height) - toolbarHeight
            var totalY = 0
            var lastProgress = -1.0f

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                totalY += dy
                val progress = Math.min(totalY / scrollableSpace, 1.0f)
                if (progress != lastProgress) {
                    lastProgress = progress
                    iv_logo.scaleX = 1.0f - 0.4f * progress * progress
                    iv_logo.scaleY = iv_logo.scaleX
                    iv_logo.translationY = (toolbarHeight - iv_logo.height) / 2.0f * progress
                    toolbar_background.alpha =
                            Math.pow(Math.max(((progress - 0.6f) / 0.4f), 0.0f).toDouble(), 2.0).toFloat()
                }
            }

        })

        swipe_refresh_layout.setColorSchemeResources(R.color.colorAccent)
        swipe_refresh_layout.setOnRefreshListener {
            mainViewModel.getRockets(false)
        }

        iv_filter.setOnClickListener { view ->
            if (view != null) {
                val popup = PopupMenu(this@MainActivity, view)
                popup.setOnMenuItemClickListener(this@MainActivity as PopupMenu.OnMenuItemClickListener)
                popup.inflate(R.menu.menu_main_filter_rockets)
                popup.show()
            }
        }

        observeWelcomeDialogDisplayed()
        observeGetRockets(adapter)

        mainViewModel.getWelcomeDialogDisplayed()
        mainViewModel.getRockets()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == detailsRequestCode) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val updatedRocket = data.getParcelableExtra<Rocket>(DetailsActivity.extraRocket)
                val rocket = mainViewModel.rockets.value?.firstOrNull { it.rocketId == updatedRocket.rocketId }
                rocket?.launches = updatedRocket.launches
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        if (item != null) {
            filter = when (item.itemId) {
                R.id.menu_item_active -> Filter.ACTIVE
                R.id.menu_item_inactive -> Filter.INACTIVE
                else -> Filter.ALL
            }
            val rockets = mainViewModel.rockets.value
            if (rockets != null) {
                (rv_content.adapter as MainAdapter).loadRockets(applyFilter(rockets))
            }
            return true
        }
        return false
    }

    private fun observeWelcomeDialogDisplayed() {
        mainViewModel.welcomeDialogDisplayed.observe(this, Observer {
            if (!it) {
                Handler().postDelayed({
                    DialogFactory.displayWelcomeDialog(this)
                    mainViewModel.setWelcomeDialogDisplayed()
                }, 2000)
            }
        })
    }

    private fun observeGetRockets(adapter: MainAdapter) {
        mainViewModel.rockets.observe(this, Observer {
            adapter.loadRockets(applyFilter(it))
            if (swipe_refresh_layout.isRefreshing) {
                swipe_refresh_layout.isRefreshing = false
            }
        })
        mainViewModel.error.observe(this, Observer {
            DialogFactory.displayGenericErrorDialog(this)
            if (swipe_refresh_layout.isRefreshing) {
                swipe_refresh_layout.isRefreshing = false
            }
        })
        mainViewModel.noInternet.observe(this, Observer {
            DialogFactory.displayNoInternetDialog(this)
            if (swipe_refresh_layout.isRefreshing) {
                swipe_refresh_layout.isRefreshing = false
            }
        })
    }

    private fun applyFilter(rockets: List<Rocket>): List<Rocket> {
        return when (filter) {
            Filter.ACTIVE -> rockets.filter { it.active }
            Filter.INACTIVE -> rockets.filter { !it.active }
            else -> rockets
        }
    }
}