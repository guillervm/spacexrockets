package com.guillervm.spacexrockets.view.details

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_details_year_header.view.*

class YearHeaderViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    var year: String? = null
        set(value) {
            field = value
            view.tv_year.text = value ?: ""
        }

}