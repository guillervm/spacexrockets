package com.guillervm.spacexrockets.view.details

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.guillervm.spacexrockets.R
import com.guillervm.spacexrockets.common.GlideApp
import kotlinx.android.synthetic.main.item_details_launch.view.*

class LaunchViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    var patch: String? = null
        set(value) {
            field = value
            GlideApp.with(view).load(value).placeholder(R.drawable.patch_placeholder).into(view.iv_patch)
        }

    var success: Boolean? = null
        set(value) {
            field = value
            view.iv_success.setImageResource(
                when (value) {
                    true -> R.drawable.circle_success
                    false -> R.drawable.circle_failure
                    else -> R.drawable.circle_unknown
                }
            )
        }

    var missionName: String? = null
        set(value) {
            field = value
            view.tv_mission_name.text = value ?: "?"
        }

    var launchDate: String? = null
        set(value) {
            field = value
            view.tv_launch_date.text = value ?: "?"
        }

}