package com.guillervm.spacexrockets.view.details

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.guillervm.spacexrockets.R
import kotlinx.android.synthetic.main.item_details_description.view.*

class DescriptionViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    var description: String? = null
        set(value) {
            field = value
            view.tv_description.text = value ?: view.resources.getString(R.string.description_not_available)
        }

}