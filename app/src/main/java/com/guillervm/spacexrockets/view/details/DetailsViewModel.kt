package com.guillervm.spacexrockets.view.details

import androidx.lifecycle.MutableLiveData
import com.guillervm.spacexrockets.common.BaseViewModel
import com.guillervm.spacexrockets.data.api.ApiObserver
import com.guillervm.spacexrockets.data.model.ErrorData
import com.guillervm.spacexrockets.data.model.Launch
import com.guillervm.spacexrockets.data.model.Rocket
import com.guillervm.spacexrockets.data.repository.SpaceXRepository
import com.guillervm.spacexrockets.util.NetworkUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class DetailsViewModel @Inject constructor(
    networkUtil: NetworkUtil,
    private val spaceXRepository: SpaceXRepository
) : BaseViewModel(networkUtil) {

    var launches: MutableLiveData<List<Launch>> = MutableLiveData()

    fun getAllLaunchesForRocket(rocket: Rocket) {
        if (rocket.launches != null) {
            launches.value = rocket.launches
        } else {
            if (networkUtil.isConnected()) {
                loader.value = true
                spaceXRepository.getAllLaunchesForRocket(rocket.rocketId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : ApiObserver<List<Launch>>(compositeDisposable) {
                        override fun onData(data: List<Launch>) {
                            loader.value = false
                            launches.value = data
                        }

                        override fun onSuccess() {
                            //
                        }

                        override fun onError(e: ErrorData) {
                            loader.value = false
                            error.value = e
                        }
                    })
            } else {
                noInternet.value = true
            }
        }
    }
}