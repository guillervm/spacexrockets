package com.guillervm.spacexrockets.view.details

import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.db.chart.animation.Animation
import com.db.chart.model.LineSet
import com.guillervm.spacexrockets.R
import kotlinx.android.synthetic.main.item_details_graph.view.*

class GraphViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    var lineColor: Int = ContextCompat.getColor(view.context, R.color.sky)

    var launchesPerYear: Map<String, Int>? = null
        set(value) {
            field = value
            val lineSet = LineSet()
            lineSet.isSmooth = true
            lineSet.color = lineColor
            if (launchesPerYear != null && !launchesPerYear!!.isEmpty()) {
                val max = launchesPerYear!!.values.max()
                if (max != null) {
                    for (divisor in listOf(3, 4, 5, 2, 1)) {
                        if (max % divisor == 0) {
                            view.chart_launches.setStep(max / divisor)
                            break
                        }
                    }
                } else {
                    view.chart_launches.setStep(1)
                }
                for ((year, launches) in launchesPerYear!!) {
                    lineSet.addPoint(year, launches.toFloat())
                }
            } else {
                view.chart_launches.setStep(1)
            }
            view.chart_launches.data.clear()
            view.chart_launches.show()
            view.chart_launches.data.add(lineSet)
            view.chart_launches.show(animation)
        }

    companion object {
        val animation = Animation(500).apply {
            this.setInterpolator(DecelerateInterpolator())
        }
    }

}