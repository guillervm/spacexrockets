package com.guillervm.spacexrockets.view.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.guillervm.spacexrockets.R
import com.guillervm.spacexrockets.data.model.Launch
import com.guillervm.spacexrockets.view.main.FooterViewHolder
import java.text.SimpleDateFormat
import java.util.*

class DetailsAdapter(val description: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<DataItem> = mutableListOf()

    fun loadLaunches(launches: List<Launch>) {
        items.clear()

        if (!launches.isEmpty()) {
            val orderedLaunches = launches.sortedByDescending { it.launchDateUnix }

            items.add(DataGraph(orderedLaunches.reversed().groupingBy { it.launchYear }.eachCount()))

            val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
            var previousYear = ""
            for (launch in orderedLaunches) {
                if (previousYear != launch.launchYear) {
                    items.add(DataYearHeader(launch.launchYear))
                    previousYear = launch.launchYear
                }
                items.add(
                    DataLaunch(
                        launch.links.missionPatchSmall,
                        launch.launchSuccess,
                        launch.missionName,
                        dateFormat.format(Date(launch.launchDateUnix * 1000))
                    )
                )
            }
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (!items.isEmpty()) {
            items.size + 3 // Description + Header + Items + Footer
        } else {
            4 // Description + Header + Placeholder + Footer
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            viewTypeDescription
        } else if (position == 1) {
            viewTypeHeader
        } else if (items.isEmpty()) {
            if (position == 2) {
                viewTypeNoLaunchesPlaceholder
            } else {
                viewTypeFooter
            }
        } else {
            if (position < items.size + 2) {
                items[position - 2].getViewType()
            } else {
                viewTypeFooter
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            viewTypeLaunch -> LaunchViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_details_launch,
                    parent,
                    false
                )
            )
            viewTypeDescription -> DescriptionViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_details_description,
                    parent,
                    false
                )
            )
            viewTypeHeader -> FooterViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_details_header,
                    parent,
                    false
                )
            )
            viewTypeFooter -> FooterViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_details_footer,
                    parent,
                    false
                )
            )
            viewTypeYearHeader -> YearHeaderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_details_year_header,
                    parent,
                    false
                )
            )
            viewTypeGraph -> GraphViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_details_graph,
                    parent,
                    false
                )
            )
            else -> NoLaunchesPlaceholderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_details_no_launches_placeholder,
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            viewTypeDescription -> (holder as DescriptionViewHolder).description = description
            viewTypeLaunch -> {
                val item = getDataItemForPosition(position) as DataLaunch
                (holder as LaunchViewHolder).apply {
                    holder.patch = item.patch
                    holder.success = item.success
                    holder.missionName = item.missionName
                    holder.launchDate = item.launchDate
                }
            }
            viewTypeYearHeader -> (holder as YearHeaderViewHolder).year =
                    (getDataItemForPosition(position) as DataYearHeader).year
            viewTypeGraph -> (holder as GraphViewHolder).launchesPerYear =
                    (getDataItemForPosition(position) as DataGraph).launchesPerYear
        }
    }

    private fun getDataItemForPosition(position: Int): DataItem {
        return items[position - 2]
    }

    private abstract class DataItem {

        abstract fun getViewType(): Int

    }

    private class DataGraph(val launchesPerYear: Map<String, Int>) : DataItem() {

        override fun getViewType() = viewTypeGraph

    }

    private class DataYearHeader(val year: String) : DataItem() {

        override fun getViewType() = viewTypeYearHeader

    }

    private class DataLaunch(
        val patch: String?,
        val success: Boolean?,
        val missionName: String,
        val launchDate: String
    ) : DataItem() {

        override fun getViewType() = viewTypeLaunch

    }

    companion object {
        private const val viewTypeDescription = 0
        private const val viewTypeHeader = 1
        private const val viewTypeNoLaunchesPlaceholder = 2
        private const val viewTypeGraph = 3
        private const val viewTypeYearHeader = 4
        private const val viewTypeLaunch = 5
        private const val viewTypeFooter = 6
    }
}