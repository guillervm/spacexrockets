package com.guillervm.spacexrockets.view.details

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class HeaderViewHolder(val view: View) : RecyclerView.ViewHolder(view)