package com.guillervm.spacexrockets.view.main

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.guillervm.spacexrockets.data.model.Rocket
import kotlinx.android.synthetic.main.item_main_rocket.view.*

class RocketViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    var rocket: Rocket? = null
        set(value) {
            field = value
            if (value != null) {
                view.tv_name.text = value.rocketName
                view.tv_country.text = value.country
                view.tv_engines.text = value.engines.number.toString()
            } else {
                view.tv_name.text = "?"
                view.tv_country.text = "?"
                view.tv_engines.text = "?"
            }
        }

}