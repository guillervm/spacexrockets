package com.guillervm.spacexrockets.view.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.guillervm.spacexrockets.R
import com.guillervm.spacexrockets.data.model.Rocket

class MainAdapter(val onClick: (Rocket) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var rockets: List<Rocket> = emptyList()

    fun loadRockets(newRockets: List<Rocket>) {
        rockets = newRockets
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (!rockets.isEmpty()) {
            rockets.size + 2 // Header + Rockets + Footer
        } else {
            3 // Header + Placeholder + Footer
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            viewTypeHeader
        } else if (rockets.isEmpty()) {
            if (position == 1) viewTypeNoRocketsPlaceHolder else viewTypeFooter
        } else {
            if (position <= rockets.size) viewTypeRocket else viewTypeFooter
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            viewTypeRocket -> RocketViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_main_rocket,
                    parent,
                    false
                )
            )
            viewTypeHeader -> HeaderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_main_header,
                    parent,
                    false
                )
            )
            viewTypeFooter -> FooterViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_main_footer,
                    parent,
                    false
                )
            )
            else -> NoRocketsPlaceholderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_main_no_rockets_placeholder,
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == viewTypeRocket) {
            (holder as RocketViewHolder).rocket = getRocketForPosition(position)
            holder.view.setOnClickListener { onClick(getRocketForPosition(position)) }
        }
    }

    private fun getRocketForPosition(position: Int): Rocket {
        return rockets[position - 1]
    }

    companion object {
        private const val viewTypeHeader = 0
        private const val viewTypeRocket = 1
        private const val viewTypeNoRocketsPlaceHolder = 2
        private const val viewTypeFooter = 3
    }
}