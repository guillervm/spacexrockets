package com.guillervm.spacexrockets.view.details

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.guillervm.spacexrockets.R
import com.guillervm.spacexrockets.common.BaseActivity
import com.guillervm.spacexrockets.data.model.Rocket
import com.guillervm.spacexrockets.util.DialogFactory
import kotlinx.android.synthetic.main.activity_details.*
import javax.inject.Inject

class DetailsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var rocket: Rocket
    private lateinit var detailsViewModel: DetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        rocket = intent.getParcelableExtra(extraRocket)

        detailsViewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailsViewModel::class.java)
        observeLoader(detailsViewModel)

        tv_details_name.text = rocket.rocketName

        val adapter = DetailsAdapter(rocket.description)

        rv_content_details.layoutManager = LinearLayoutManager(this).apply { orientation = RecyclerView.VERTICAL }
        rv_content_details.adapter = adapter
        val ta = obtainStyledAttributes(intArrayOf(R.attr.actionBarSize))
        val toolbarHeight = ta.getDimensionPixelSize(0, -1)
        ta.recycle()
        rv_content_details.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            val scrollableSpace = resources.getDimension(R.dimen.description_name_height) - toolbarHeight
            var totalY = 0
            var lastProgress = -1.0f

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                totalY += dy
                val progress = Math.min(totalY / scrollableSpace, 1.0f)
                if (progress != lastProgress) {
                    lastProgress = progress
                    tv_details_name.scaleX = 1.0f - 0.4f * progress * progress
                    tv_details_name.scaleY = tv_details_name.scaleX
                    tv_details_name.translationY = (toolbarHeight - tv_details_name.height) / 2.0f * progress
                    toolbar_background.alpha =
                            Math.pow(Math.max(((progress - 0.6f) / 0.4f), 0.0f).toDouble(), 2.0).toFloat()
                }
            }

        })

        observeGetAllLaunchesForRocket(adapter)

        detailsViewModel.getAllLaunchesForRocket(rocket)
    }

    override fun onBackPressed() {
        if (rocket.launches != null) {
            setResult(Activity.RESULT_OK, Intent().putExtra(extraRocket, rocket))
        } else {
            setResult(Activity.RESULT_CANCELED)
        }
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun observeGetAllLaunchesForRocket(adapter: DetailsAdapter) {
        detailsViewModel.launches.observe(this, Observer {
            rocket.launches = it
            adapter.loadLaunches(it)
        })
        detailsViewModel.error.observe(this, Observer {
            DialogFactory.displayGenericErrorDialog(this)
        })
        detailsViewModel.noInternet.observe(this, Observer {
            DialogFactory.displayNoInternetDialog(this)
        })
    }

    companion object {

        val extraRocket = "extra_rocket"

        fun newIntent(context: Context, rocket: Rocket): Intent {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra(extraRocket, rocket)
            return intent
        }
    }
}