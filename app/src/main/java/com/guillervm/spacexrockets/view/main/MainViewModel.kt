package com.guillervm.spacexrockets.view.main

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import com.guillervm.spacexrockets.common.BaseViewModel
import com.guillervm.spacexrockets.data.api.ApiObserver
import com.guillervm.spacexrockets.data.local.PreferencesHelper
import com.guillervm.spacexrockets.data.model.ErrorData
import com.guillervm.spacexrockets.data.model.Rocket
import com.guillervm.spacexrockets.data.repository.SpaceXRepository
import com.guillervm.spacexrockets.util.NetworkUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(
    networkUtil: NetworkUtil,
    private val preferencesHelper: PreferencesHelper,
    private val spaceXRepository: SpaceXRepository
) : BaseViewModel(networkUtil) {

    var welcomeDialogDisplayed: MutableLiveData<Boolean> = MutableLiveData(false)
    var rockets: MutableLiveData<List<Rocket>> = MutableLiveData()

    fun getWelcomeDialogDisplayed() {
        welcomeDialogDisplayed.value = preferencesHelper.getWelcomeMessageDisplayed()
    }

    fun setWelcomeDialogDisplayed() {
        preferencesHelper.setWelcomeMessageDisplayed()
        welcomeDialogDisplayed.value = true
    }

    fun getRockets(showLoading: Boolean = true) {
        if (networkUtil.isConnected()) {
            if (showLoading) {
                loader.value = true
            }
            Handler().postDelayed({
                spaceXRepository.getRockets()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : ApiObserver<List<Rocket>>(compositeDisposable) {
                        override fun onData(data: List<Rocket>) {
                            loader.value = false
                            rockets.value = data
                        }

                        override fun onSuccess() {
                            //
                        }

                        override fun onError(e: ErrorData) {
                            loader.value = false
                            error.value = e
                        }
                    })
            }, 2000)
        } else {
            noInternet.value = true
        }
    }
}