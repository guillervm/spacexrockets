package com.guillervm.spacexrockets.common

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.guillervm.spacexrockets.R
import com.guillervm.spacexrockets.view.loading.LoadingDialog
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (isThemeFullScreen()) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
    }

    fun observeLoader(viewModel: BaseViewModel) {
        viewModel.loader.observe(this, Observer {
            if (it != null && it) showLoading() else hideLoading()
        })
    }

    private fun isThemeFullScreen(): Boolean {
        try {
            val wrapper = Context::class.java
            val method = wrapper.getMethod("getThemeResId")
            method.isAccessible = true
            val id = method.invoke(this) as Int
            return fullScreenStyleIds.contains(id)
        } catch (e: Exception) {
            //
        }
        return false
    }

    private fun showLoading() {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        val previous = fragmentManager.findFragmentByTag(loadingFragmentTag)
        if (previous == null || !previous.isAdded) {
            transaction.addToBackStack(null)
            transaction.add(LoadingDialog.newInstance(), loadingFragmentTag)
            transaction.commit()
        }
    }

    private fun hideLoading() {
        val fragmentManager = supportFragmentManager
        val previous: DialogFragment? = fragmentManager.findFragmentByTag(loadingFragmentTag) as DialogFragment?
        previous?.dismissAllowingStateLoss()
    }

    companion object {
        private const val loadingFragmentTag = "loading_fragment_id"
        private val fullScreenStyleIds = intArrayOf(R.style.AppTheme_FullScreen)
    }
}