package com.guillervm.spacexrockets.common

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.guillervm.spacexrockets.data.model.ErrorData
import com.guillervm.spacexrockets.util.NetworkUtil
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel(val networkUtil: NetworkUtil) : ViewModel() {

    val loader: MutableLiveData<Boolean> = MutableLiveData()
    val error: MutableLiveData<ErrorData?> = MutableLiveData()
    val noInternet: MutableLiveData<Boolean> = MutableLiveData()
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}