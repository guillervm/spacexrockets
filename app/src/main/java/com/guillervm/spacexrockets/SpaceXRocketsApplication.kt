package com.guillervm.spacexrockets

import com.guillervm.spacexrockets.ioc.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication


class SpaceXRocketsApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out SpaceXRocketsApplication> {
        return DaggerAppComponent.builder().create(this)
    }
}